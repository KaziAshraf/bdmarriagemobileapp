﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using Android.Webkit;

namespace BDMarriageMobile
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            WebView view = FindViewById<WebView>(Resource.Id.webloader);
            view.SetWebViewClient(new WebViewClientClass());
            view.LoadUrl("http://www.bdmarriage.com/");
            WebSettings settings = view.Settings;
            settings.JavaScriptEnabled = true;

        }
    }

    internal class WebViewClientClass : WebViewClient
    {
        public override bool ShouldOverrideUrlLoading(WebView view, string url)
        {
            view.LoadUrl(url);
            return true;
        }
    }
}

